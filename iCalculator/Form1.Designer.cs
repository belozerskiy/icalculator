﻿namespace iCalculator
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainForm));
            this.first = new System.Windows.Forms.Button();
            this.fourth = new System.Windows.Forms.Button();
            this.seventh = new System.Windows.Forms.Button();
            this.second = new System.Windows.Forms.Button();
            this.fifth = new System.Windows.Forms.Button();
            this.eighth = new System.Windows.Forms.Button();
            this.third = new System.Windows.Forms.Button();
            this.sixth = new System.Windows.Forms.Button();
            this.ninth = new System.Windows.Forms.Button();
            this.zero = new System.Windows.Forms.Button();
            this.coma = new System.Windows.Forms.Button();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.devide = new System.Windows.Forms.Button();
            this.multiply = new System.Windows.Forms.Button();
            this.minus = new System.Windows.Forms.Button();
            this.plus = new System.Windows.Forms.Button();
            this.sqrt = new System.Windows.Forms.Button();
            this.percent = new System.Windows.Forms.Button();
            this.equally = new System.Windows.Forms.Button();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.viewToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.normalToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.engineerToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.bitToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.helpToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.aboutUsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.menuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // first
            // 
            this.first.Location = new System.Drawing.Point(14, 81);
            this.first.Name = "first";
            this.first.Size = new System.Drawing.Size(38, 32);
            this.first.TabIndex = 0;
            this.first.Text = "1";
            this.first.UseVisualStyleBackColor = true;
            this.first.Click += new System.EventHandler(this.button1_Click);
            // 
            // fourth
            // 
            this.fourth.Location = new System.Drawing.Point(14, 119);
            this.fourth.Name = "fourth";
            this.fourth.Size = new System.Drawing.Size(38, 32);
            this.fourth.TabIndex = 0;
            this.fourth.Text = "4";
            this.fourth.UseVisualStyleBackColor = true;
            // 
            // seventh
            // 
            this.seventh.Location = new System.Drawing.Point(14, 157);
            this.seventh.Name = "seventh";
            this.seventh.Size = new System.Drawing.Size(38, 32);
            this.seventh.TabIndex = 0;
            this.seventh.Text = "7";
            this.seventh.UseVisualStyleBackColor = true;
            // 
            // second
            // 
            this.second.Location = new System.Drawing.Point(57, 81);
            this.second.Name = "second";
            this.second.Size = new System.Drawing.Size(38, 32);
            this.second.TabIndex = 0;
            this.second.Text = "2";
            this.second.UseVisualStyleBackColor = true;
            // 
            // fifth
            // 
            this.fifth.Location = new System.Drawing.Point(57, 119);
            this.fifth.Name = "fifth";
            this.fifth.Size = new System.Drawing.Size(38, 32);
            this.fifth.TabIndex = 0;
            this.fifth.Text = "5";
            this.fifth.UseVisualStyleBackColor = true;
            // 
            // eighth
            // 
            this.eighth.Location = new System.Drawing.Point(57, 157);
            this.eighth.Name = "eighth";
            this.eighth.Size = new System.Drawing.Size(38, 32);
            this.eighth.TabIndex = 0;
            this.eighth.Text = "8";
            this.eighth.UseVisualStyleBackColor = true;
            // 
            // third
            // 
            this.third.Location = new System.Drawing.Point(101, 81);
            this.third.Name = "third";
            this.third.Size = new System.Drawing.Size(38, 32);
            this.third.TabIndex = 0;
            this.third.Text = "3";
            this.third.UseVisualStyleBackColor = true;
            // 
            // sixth
            // 
            this.sixth.Location = new System.Drawing.Point(101, 119);
            this.sixth.Name = "sixth";
            this.sixth.Size = new System.Drawing.Size(38, 32);
            this.sixth.TabIndex = 0;
            this.sixth.Text = "6";
            this.sixth.UseVisualStyleBackColor = true;
            // 
            // ninth
            // 
            this.ninth.Location = new System.Drawing.Point(101, 157);
            this.ninth.Name = "ninth";
            this.ninth.Size = new System.Drawing.Size(38, 32);
            this.ninth.TabIndex = 0;
            this.ninth.Text = "9";
            this.ninth.UseVisualStyleBackColor = true;
            // 
            // zero
            // 
            this.zero.Location = new System.Drawing.Point(14, 193);
            this.zero.Name = "zero";
            this.zero.Size = new System.Drawing.Size(81, 32);
            this.zero.TabIndex = 0;
            this.zero.Text = "0";
            this.zero.UseVisualStyleBackColor = true;
            // 
            // coma
            // 
            this.coma.Location = new System.Drawing.Point(101, 193);
            this.coma.Name = "coma";
            this.coma.Size = new System.Drawing.Size(38, 32);
            this.coma.TabIndex = 0;
            this.coma.Text = ",";
            this.coma.UseVisualStyleBackColor = true;
            // 
            // textBox1
            // 
            this.textBox1.Font = new System.Drawing.Font("Microsoft Sans Serif", 18.25F);
            this.textBox1.Location = new System.Drawing.Point(14, 33);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(213, 35);
            this.textBox1.TabIndex = 1;
            // 
            // devide
            // 
            this.devide.Location = new System.Drawing.Point(145, 81);
            this.devide.Name = "devide";
            this.devide.Size = new System.Drawing.Size(38, 32);
            this.devide.TabIndex = 0;
            this.devide.Text = "/";
            this.devide.UseVisualStyleBackColor = true;
            // 
            // multiply
            // 
            this.multiply.Location = new System.Drawing.Point(145, 119);
            this.multiply.Name = "multiply";
            this.multiply.Size = new System.Drawing.Size(38, 32);
            this.multiply.TabIndex = 0;
            this.multiply.Text = "*";
            this.multiply.UseVisualStyleBackColor = true;
            // 
            // minus
            // 
            this.minus.Location = new System.Drawing.Point(145, 157);
            this.minus.Name = "minus";
            this.minus.Size = new System.Drawing.Size(38, 32);
            this.minus.TabIndex = 0;
            this.minus.Text = "-";
            this.minus.UseVisualStyleBackColor = true;
            // 
            // plus
            // 
            this.plus.Location = new System.Drawing.Point(145, 193);
            this.plus.Name = "plus";
            this.plus.Size = new System.Drawing.Size(38, 32);
            this.plus.TabIndex = 0;
            this.plus.Text = "+";
            this.plus.UseVisualStyleBackColor = true;
            // 
            // sqrt
            // 
            this.sqrt.Location = new System.Drawing.Point(189, 81);
            this.sqrt.Name = "sqrt";
            this.sqrt.Size = new System.Drawing.Size(38, 32);
            this.sqrt.TabIndex = 0;
            this.sqrt.Text = "√";
            this.sqrt.UseVisualStyleBackColor = true;
            // 
            // percent
            // 
            this.percent.Location = new System.Drawing.Point(189, 119);
            this.percent.Name = "percent";
            this.percent.Size = new System.Drawing.Size(38, 32);
            this.percent.TabIndex = 0;
            this.percent.Text = "%";
            this.percent.UseVisualStyleBackColor = true;
            // 
            // equally
            // 
            this.equally.Location = new System.Drawing.Point(189, 157);
            this.equally.Name = "equally";
            this.equally.Size = new System.Drawing.Size(38, 68);
            this.equally.TabIndex = 0;
            this.equally.Text = "=";
            this.equally.UseVisualStyleBackColor = true;
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.viewToolStripMenuItem,
            this.helpToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(240, 24);
            this.menuStrip1.TabIndex = 2;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // viewToolStripMenuItem
            // 
            this.viewToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.normalToolStripMenuItem,
            this.engineerToolStripMenuItem,
            this.bitToolStripMenuItem});
            this.viewToolStripMenuItem.Name = "viewToolStripMenuItem";
            this.viewToolStripMenuItem.Size = new System.Drawing.Size(44, 20);
            this.viewToolStripMenuItem.Text = "View";
            // 
            // normalToolStripMenuItem
            // 
            this.normalToolStripMenuItem.Name = "normalToolStripMenuItem";
            this.normalToolStripMenuItem.Size = new System.Drawing.Size(152, 22);
            this.normalToolStripMenuItem.Text = "Normal";
            // 
            // engineerToolStripMenuItem
            // 
            this.engineerToolStripMenuItem.Name = "engineerToolStripMenuItem";
            this.engineerToolStripMenuItem.Size = new System.Drawing.Size(152, 22);
            this.engineerToolStripMenuItem.Text = "Engineer";
            // 
            // bitToolStripMenuItem
            // 
            this.bitToolStripMenuItem.Name = "bitToolStripMenuItem";
            this.bitToolStripMenuItem.Size = new System.Drawing.Size(152, 22);
            this.bitToolStripMenuItem.Text = "Bit";
            // 
            // helpToolStripMenuItem
            // 
            this.helpToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.aboutUsToolStripMenuItem});
            this.helpToolStripMenuItem.Name = "helpToolStripMenuItem";
            this.helpToolStripMenuItem.Size = new System.Drawing.Size(44, 20);
            this.helpToolStripMenuItem.Text = "Help";
            // 
            // aboutUsToolStripMenuItem
            // 
            this.aboutUsToolStripMenuItem.Name = "aboutUsToolStripMenuItem";
            this.aboutUsToolStripMenuItem.Size = new System.Drawing.Size(152, 22);
            this.aboutUsToolStripMenuItem.Text = "About us";
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(240, 237);
            this.Controls.Add(this.textBox1);
            this.Controls.Add(this.equally);
            this.Controls.Add(this.plus);
            this.Controls.Add(this.minus);
            this.Controls.Add(this.coma);
            this.Controls.Add(this.ninth);
            this.Controls.Add(this.eighth);
            this.Controls.Add(this.percent);
            this.Controls.Add(this.zero);
            this.Controls.Add(this.multiply);
            this.Controls.Add(this.seventh);
            this.Controls.Add(this.sixth);
            this.Controls.Add(this.sqrt);
            this.Controls.Add(this.fifth);
            this.Controls.Add(this.devide);
            this.Controls.Add(this.fourth);
            this.Controls.Add(this.third);
            this.Controls.Add(this.second);
            this.Controls.Add(this.first);
            this.Controls.Add(this.menuStrip1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "MainForm";
            this.Text = "Form1";
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button first;
        private System.Windows.Forms.Button fourth;
        private System.Windows.Forms.Button seventh;
        private System.Windows.Forms.Button second;
        private System.Windows.Forms.Button fifth;
        private System.Windows.Forms.Button eighth;
        private System.Windows.Forms.Button third;
        private System.Windows.Forms.Button sixth;
        private System.Windows.Forms.Button ninth;
        private System.Windows.Forms.Button zero;
        private System.Windows.Forms.Button coma;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.Button devide;
        private System.Windows.Forms.Button multiply;
        private System.Windows.Forms.Button minus;
        private System.Windows.Forms.Button plus;
        private System.Windows.Forms.Button sqrt;
        private System.Windows.Forms.Button percent;
        private System.Windows.Forms.Button equally;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem viewToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem normalToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem engineerToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem bitToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem helpToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem aboutUsToolStripMenuItem;
    }
}

